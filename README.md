### Project 3: Vue.js Calendar

#### Installation

1. Clone this repository on your local file system

    ```
    cd /path/to/install/location
    git clone git@github.com:getjsdojo/vuejs-calendar.git
    ```

2. Install dependencies

    ```
    npm install
    ```

3. Create a `.env` file by copying the sample

    ```
    cp .env_sample .env
    ```
    
    Edit the .env file and replace any variables if needed
    
4. Start project

    ```
    npm run start
    ```

5. Your site will be available at *localhost:[PORT]* where `PORT` is whatever value is set in your `.env` file.



